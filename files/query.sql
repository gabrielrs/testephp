use music;
SELECT
    songs.*,
    (SELECT
            COUNT(playlist_songs.songs_id)
        FROM
            music.playlist_songs
        WHERE
            playlist_id = 2
        GROUP BY songs.id) AS total_tracks
FROM
    music.playlist_songs AS playlist_songs
        INNER JOIN
    music.songs AS songs ON songs.id = playlist_songs.songs_id
WHERE
    playlist_id = 2
ORDER BY songs.artist_id , songs.albums_id