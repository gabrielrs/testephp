<?php

$products = [
    [
        "title" => "Mouse",
        "amount" => 3,
        "sold" => 42,
    ],
    [
        "title" => "Headset",
        "amount" => 2,
        "sold" => 150,
    ],
    [
        "title" => "Keyboard",
        "amount" => 5,
        "sold" => 70,
    ],
];

$alertTextStock = function (array $item) {
    echo "The product " . $item['title'] . " has only " . $item['amount'] . " items in stock! \n";
};

$checksStock = function ($item) use ($alertTextStock) {
    return $item['amount'] > 4 ?: $alertTextStock($item);
};


array_map($checksStock, $products);