<?php
trait FormatText
{
    public function removeNumber(String $text)
    {
        return preg_replace('/[0-9]/', '', $text);
    }
}

class ShowText
{
    use FormatText;
    private String $text;

    public function __construct()
    {
        $this->text = 'Example 578 Text 2020';
    }

    public function exampleMethod()
    {
        echo $this->text;
    }

    public function exampleFormatTrait()
    {
        echo $this->removeNumber($this->text);
    }
}

$showText = new ShowText();
$showText->exampleMethod(); // Example 578 Text 2020
$showText->exampleFormatTrait(); // Example Text
