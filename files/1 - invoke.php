<?php

class Example
{
    private String $text;

    public function __construct()
    {
        $this->text = 'Text';
    }

    public function __invoke()
    {
        echo 'Invoke ' . $this->text;
    }

    public function method()
    {
        echo 'Method ' . $this->text;
    }
}

$example = new Example;

// Calling instance method
$example->method(); // Method Text

// Calling instance as function (__invoke magic method)
$example(); // Invoke Text