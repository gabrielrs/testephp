# Teste PHP - Digital Business

Você pode usar http://phptester.net/ para testar os códigos php aqui apresentados.

### 1) Explique e faça um exemplo do método __invoke()
O método __invoke é um método mágico do php, ele é chamado quando chamamos um objeto como função. Compare as saídas no exemplo:
```php
<?php
class Example
{
	private String $text;
	
	public function __construct()
    {
		$this->text = 'Text';
    }
	
    public function __invoke()
    {
        echo 'Invoke ' . $this->text;
    }
	
	public function method()
	{
		echo 'Method ' . $this->text;
	}
}

$example = new Example;

// Calling instance method
$example->method(); // Method Text

// Calling instance as function (__invoke magic method)
$example(); // Invoke Text

```


### 2) Explique e faça um exemplo aplicando Trait.
Com traits podemos reutilizar códigos evitando duplicidades, posso usar uma trait em diferentes classes reaproveitando seus métodos, resolvendo alguns desafios e limitações de herança única(Uma classe filha pode herdar apenas de um único pai).
	
Exemplo:

```php
<?php
trait FormatText
{
    public function removeNumber(String $text)
    {
        return preg_replace('/[0-9]/', '', $text);
    }
}

class ShowText
{
    use FormatText;
    private String $text;

    public function __construct()
    {
        $this->text = 'Example 578 Text 2020';
    }

    public function exampleMethod()
    {
        echo $this->text;
    }

    public function exampleFormatTrait()
    {
        echo $this->removeNumber($this->text);
    }
}

$showText = new ShowText();
$showText->exampleMethod(); // Example 578 Text 2020
$showText->exampleFormatTrait(); // Example Text
```

### 3) Cite a função de cada um dos seguintes métodos HTTP
- a. GET

Serve para solicitar a representação de um dado/recurso.

- b. POST

Utilizado para criar novos recursos, nesse método os dados vão no corpo da requisição.

- c. PUT

Serve para criar ou editar um recurso, ou seja, caso o recurso que esteja sendo enviado no momento exista ele atualiza todo o recurso, se não existir ele insere esse novo registro.

- d. DELETE

Usamos esse método para deletar um recurso, devemos informar um identificador para essa ação.

- e. PATCH

Com o método patch, podemos atualizamos um recurso em parte.

### 4) Faça um código, usando uma função anônima, que retorne uma lista de itens com até 3 níveis.
Exemplo:
- item 1
- item 2
- - item 2.1
- item 3
- item 4
- -  item 4.1
- - - item 4.2

Eu não entendi direito o que era esperado, resolvi fazer um exemplo com função anônima que são funções muito usadas para callbacks, assim não tendo uma função somente para um uso, podendo reutilizar em mais de um local, geralmente atribuímos essas funções a uma variável, veja o exemplo:


```php
<?php

$products = [
    [
        "title" => "Mouse",
        "amount" => 3,
        "sold" => 42,
    ],
    [
        "title" => "Headset",
        "amount" => 2,
        "sold" => 150,
    ],
    [
        "title" => "Keyboard",
        "amount" => 5,
        "sold" => 70,
    ],
];

$alertTextStock = function (array $item) {
    echo "The product " . $item['title'] . " has only " . $item['amount'] . " items in stock! \n";
};

$checksStock = function ($item) use ($alertTextStock) {
    return $item['amount'] > 4 ?: $alertTextStock($item);
};


array_map($checksStock, $products);
```

### 5) Faça a modelagem para um sistema de músicas por streaming, onde usuários podem criar suas playlists e também favoritar as músicas.


![N|Solid](https://i.imgur.com/806AT0f.png)


### 6) Utilizando a modelagem criada na questão 5, crie uma query para listar as músicas de uma playlist com o total de músicas junto.
```sql
SELECT 
    songs.*,
    (SELECT 
            COUNT(playlist_songs.songs_id)
        FROM
            music.playlist_songs
        WHERE
            playlist_id = 2
        GROUP BY songs.id) AS total_tracks
FROM
    music.playlist_songs AS playlist_songs
        INNER JOIN
    music.songs AS songs ON songs.id = playlist_songs.songs_id
WHERE
    playlist_id = 2
ORDER BY songs.artist_id , songs.albums_id
```
Confira o resultado na próxima imagem:

![N|Solid](https://i.imgur.com/zqgLMlQ.png)
